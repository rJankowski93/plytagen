var vals = global.vals;

processAll(vals, insertDot);

putLine('C    ******* UWAGA   *** Nie usuwac linii komentarza    ');
putLine('C                                                       ');

putLine(vals.eps + "     EPS      dokladnosc rozwiazania pola tempeartury                  ");
putLine(vals.flimit + "       FLIMIT   limit wywolan normy bledu lub 1.                                ");
putLine(vals.NTPR1 + "        NTPR1    czas w po ktorym ma byc pierwszy wydruk temperatury,min");
putLine(vals.NTPR2 + "        NTPR2    przyrost czasu dla kolejnych wydrukow temperatury, min(wartosc temp beedzie wypisywana co minute)           ");
putLine(vals.NTPRE + "     NTPRE    czas po ktorym ma byc ostatni wydruk temperatury,min (przewaznie jest rowny czasowi wymiany ciepla)  ");
putLine(vals.T2SLE + "    T2SLE    czas procesu wymiany ciepla, min      ");
putLine(vals.TAMB + "    TAMB     temperatura spalin, oC       ");
putLine(vals.DTIME + "     DTIME    maksymalna dlugosc przyrostu czasu, s (bez zmian)     ");
putLine(vals.DTMIN + "    DTMIN    minimalna dlugosc przyrostu czasu,  s  (bez zmian)    ");
putLine(vals.TINIT + "      TINIT    poczatkowa temperatura plyty, oC            ");
putLine(vals.v_wlewka + "      v_wlewka objetosc wsadu, m^3   (bez zmian)       ");
putLine(vals.f_wlewka + "      f_wlewka powierzchnia wsadu,  m^2    (bez zmian)   ");
putLine(vals.h_piec + "      h_piec   wysokosc pieca, m      (bez zmian)");
putLine(vals.b_piec + "       b_piec   szerokosc pieca, m       (bez zmian)  ");
putLine(vals.w_piec + "      w_piec   dlugosc pieca,  m    (bez zmian)");
putLine(vals.coeff + "     coeff    emisyjnosc sciany pieca od stronie zewnetrznej (bez zmian)                                                                         -------------");
putLine(vals.coemssp + "     coemssp  emisyjnosc sciany pieca od stronie wewnetrznej  (bez zmian)                                                              -------------");

putLine(vals.NX1 + "    NX1    liczba elementow w kierunku X1, grubosc    |     ");
putLine(vals.NX2 + "    NX2    liczba elementow w kierunku X2, szerokosc  |       ");
putLine(vals.NX3 + "    NX3    liczba elementow w kierunku X3, dlugosc    |      ");
putLine(vals.DNP2 + "    DNP2    calkowita liczba punktow interpolacji warunkow brzegowych       ");

putLine("C-----------------------------------------------------------------------");
putLine('C        X1 - wspolrzedne wezlow                      mm   -------------');
putLine('C Wezel No:  1,     2,    3,   ....,     NX1+1             -------------');
putLine('C---------   --   --   --  --  --   --   --  --   ----------------------');

var bufline = '';

var counterX1 = parseInt(vals.NX1)+1;
var counterX2 = parseInt(vals.NX2)+1;
var counterX3 = parseInt(vals.NX3)+1;

for (var i = 0; i < counterX1; i++){
  bufline += vals.tabX1[i].toString();
  bufline += "\t";
}

putLine(bufline);

putLine("C-----------------------------------------------------------------------");
putLine('C        X2 - wspolrzedne wezlow                      mm   -------------');
putLine('C Wezel No:  1,     2,    3,   ....,     NX2+1             -------------');
putLine('C---------   --   --   --  --  --   --   --  --   ----------------------');

bufline = '';

for (var i = 0; i < counterX2; i++){
  bufline += vals.tabX2[i].toString();
  bufline += "\t";
}

putLine(bufline);

putLine("C-----------------------------------------------------------------------");
putLine('C        X3 - wspolrzedne wezlow                      mm   -------------');
putLine('C Wezel No:  1,     2,    3,   ....,     NX3+1             -------------');
putLine('C---------   --   --   --  --  --   --   --  --   ----------------------');

bufline = '';

for (var i = 0; i < counterX3; i++){
  bufline += vals.tabX3[i].toString();
  bufline += "\t";
}

putLine(bufline);

putLine('C-----------------------------------------------------------------------      ');
putLine(vals.ntpom + '      nt_pom   liczba punktow sledzenia temperatury                    ');
putLine('C-----------------------------------------------------------------------');
putLine('C        wspolrzedne X1, X2, X3 punktow sledzenia temperatury; w kolejnych wierszach');
putLine('C Punkt No:  1,     2,    3,   ...., do    nt_pom               -------------');
putLine('C---------   --   --   --  --  --   --   --  --   ----------------------  ');

// tutaj trzeba nt_pom dac
bufline  = '';
bufline2 = '';
bufline3 = '';

for (var i = 0; i < vals.ntpom; i++) {
  bufline += vals.tabNtPomX1[i].toString();
  bufline += "\t";
  bufline2 += vals.tabNtPomX2[i].toString();
  bufline2 += "\t";
  bufline3 += vals.tabNtPomX3[i].toString();
  bufline3 += "\t";
}

putLine(bufline);
putLine(bufline2);
putLine(bufline3);

putLine('C-----------------------------------------------------------------------');
putLine('C        wspolrzedne punktow interp. temp. otoczenia    min. -----------');
putLine('C Punkt No:  1,     2,    3,   ....,    DNP2               -------------');
putLine('C---------   --   --   --  --  --   --   --  --   ----------------------');
bufline = '';
for (var i = 0; i < vals.DNP2; i++) {
  bufline += vals.tabTempSur[i].toString(); // dodawanie tych kropek
  bufline += "\t";
}
putLine(bufline);
//   0.  3.0   17.8  30.3   57.8  120.2  198.5   250.0  287.6  333.0  414.0
putLine('C-----------------------------------------------------------------------');
putLine('C Wspolczynnik wymiany ciepla na zewnetrznej scianie pieca (nad plyta)--');
putLine('C przedzial No: 1,  2,    3,   ....,    DNP2                ------------');
putLine('C-----------------------------------------------------------------------');
bufline = '';
for (var i = 0; i < vals.DNP2; i++) {
  bufline += vals.tabAlfaNad[i].toString(); // dodawanie tych kropek
  bufline += "\t";
}
putLine(bufline);
//putLine(' 50. 50.  50.   50. 50.  50.  50. 50.  50.  50. 50.  50.');
putLine('C-----------------------------------------------------------------------');
putLine('C Wspolczynnik wymiany ciepla i predkosc spalin (m/s) od strony pieca (dol plyty)       (alfa pod)                --');
putLine('C przedzial No: 1,  2,    3,   ....,    DNP2               ------------');
putLine('C-----------------------------------------------------------------------');
bufline = '';
for (var i = 0; i < vals.DNP2; i++) {
  bufline += vals.tabAlfaPod[i].toString(); // dodawanie tych kropek
  bufline += "\t";
}
putLine(bufline);

bufline = '';
for (var i = 0; i < vals.DNP2; i++) {
  bufline += vals.tabSpeed[i].toString(); // dodawanie tych kropek
  bufline += "\t";
}
putLine(bufline);

putLine('C-----------------------------------------------------------------------');
putLine('C Temperatura otoczenia (nad plyta) i temperatura spalin (pod plyta)   C deg.  ');
putLine('C Punkt No:  1,     2,    3,   ....,    DNP2');
putLine('C---------   --   --   --  --  --   --   --  --   ---------------------');
// wartosci temp
bufline = '';
for (var i = 0; i < vals.DNP2; i++) {
  bufline += vals.tabTempNad[i].toString(); // dodawanie tych kropek
  bufline += "\t";
}
putLine(bufline);
// temp spalin (t pod)
bufline = '';
for (var i = 0; i < vals.DNP2; i++) {
  bufline += vals.tabTempPod[i].toString(); // dodawanie tych kropek
  bufline += "\t";
}
putLine(bufline);
putLine('C-----------------------------------------------------------------------');
putLine('C Przewodnosc cieplna warstwy elementow          W/m/K       -----------');
putLine('C Element nr:  1,     2,    3,   ....,    NX1                -----------');
putLine('C---------   --   --   --  --  --   --   --  --   ----------------------');
// wartosci przewodnosci
bufline = '';
for (var i = 0; i < vals.NX1; i++) {
  bufline += vals.tabConductivity[i].toString();
  bufline += "\t";
}
putLine(bufline);
putLine('C-----------------------------------------------------------------------');
putLine('C Gestosc materialu warstwy elementow            kg/m^3    -------------');
putLine('C Element nr:  1,     2,    3,   ....,    NX1               ------------');
putLine('C---------   --   --   --  --  --   --   --  --   ----------------------');
// wartosci gestosci
bufline = '';
for (var i = 0; i < vals.NX1; i++) {
  bufline += vals.tabDensity[i].toString();
  bufline += "\t";
}
putLine(bufline);
putLine('C-----------------------------------------------------------------------');
putLine('C Cieplo wlasciwe materialu warstwy elementow J/K*kg                  --');
putLine('C Element nr: 1,     2,    3,   ....,    Nx1;               ------------');
putLine('C---------   --   --   --  --  --   --   --  --   ----------------------');
// wartosci ciepla wl.
bufline = '';
for (var i = 0; i < vals.NX1; i++) {
  bufline += vals.tabHeatAppropriate[i].toString();
  bufline += "\t";
}
putLine(bufline);
putLine('C-END OF FILE-----------------------------------------------------------');

saveOutput('plyta_3d.inp');
