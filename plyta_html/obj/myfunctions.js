var fs = require("fs");

var vals = global.vals || {};

global.buf = global.buf || "C----Zbior danych do programu Plyta_3D  ---------------------- \n"

var putLine = function (str) {
  global.buf += str;
  global.buf += '\n';
};

var saveOutput = function (file) {
  fs.writeFile(file, global.buf,  function(err) {
    if (err) {
      return console.error(err);
    }
  });
};

var windowWidth  = 1300,
windowHeight = 700;

var changeWindow = function(name) {
  var gui = require('nw.gui');
  var win = gui.Window.get();
  var new_win = gui.Window.open(name, {
    width:  windowWidth,
    height: windowHeight
  });
  win.close();
};

var insertDot = function(num) {
  num = Number(num);
  if (Math.round(num) === num) {
    return num + '.';
  } else {
    return num;
  }
};

// ta funkcja przetwarza każdą wartosc w tablicy
// niezaleznie od tego gdzie ta wartosc jest (np. w podtablicy)
// moze sie przydac w innych projektach wiec gdzies ja mozna schowac
var processAll = function(ob, callback) {
  Object.keys(ob).forEach(function(prop) {
    if (typeof ob[prop] === 'object') {
      processAll(ob[prop], callback);
    } else {
      ob[prop] = callback(ob[prop]);
    }
  });
};

// var saveBufferState = function() {
//   global.buf = buf;
// }

// var restoreBufferState() {
//
// }
