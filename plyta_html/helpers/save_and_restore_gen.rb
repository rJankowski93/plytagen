# input array of tokens
arr = []
defVals = []

arr.push("NX1")
arr.push("NX2")
arr.push("NX3")

defVals.push(12)
defVals.push(1)
defVals.push(1)

def generateSaveFunction(arr)
  arr.each do |item|
    puts "vals."+item+" = $(\"\#"+item+"\").val();"
  end
end

def generateRestoreFunction(arr, defVals)
  arr.each_with_index do |item, i|
    puts "$(\"\#"+item+"\").val((vals."+item+" ? vals."+item+" : "+defVals[i].to_s+"));"
  end
end

generateRestoreFunction(arr, defVals)


# this for index.html
# arr = "eps
# flimit
# NTPR1
# NTPR2
# NTPRE
# T2SLE
# TAMB
# DTIME
# DTMIN
# TINIT
# v_wlewka
# f_wlewka
# h_piec
# b_piec
# w_piec
# coeff
# coemssp
# ".split("\n")
#
# defVals = []
# defVals.push(1000)
# defVals.push(1)
# defVals.push(0)
# defVals.push(1)
# defVals.push(415)
# defVals.push(415)
# defVals.push(1200)
# defVals.push(0.1)
# defVals.push(0.1)
# defVals.push(20)
# defVals.push(0.0154)
# defVals.push(0.343)
# defVals.push(0.55)
# defVals.push(0.45)
# defVals.push(0.34)
# defVals.push(0.8)
# defVals.push(0.6)
