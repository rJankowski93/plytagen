C----Zbior danych do programu Plyta_3D  ----------------------
C    ******* UWAGA   *** Nie usuwac linii komentarza    
C                                                       
1000.     EPS      dokladnosc rozwiazania pola tempeartury                  
1.       FLIMIT   limit wywolan normy bledu lub 1.                                
0        NTPR1    czas w po ktorym ma byc pierwszy wydruk temperatury,min
1        NTPR2    przyrost czasu dla kolejnych wydrukow temperatury, min           
415.     NTPRE    czas po ktorym ma byc ostatni wydruk temperatury,min   
415.    T2SLE    czas procesu wymiany ciepla, min      
1200.    TAMB     temperatura spalin, oC       
0.1     DTIME    maksymalna dlugosc przyrostu czasu, s     
0.1    DTMIN    minimalna dlugosc przyrostu czasu,  s      
17.      TINIT    poczatkowa temperatura plyty, oC           
0.0154       v_wlewka objetosc wsadu, m^3          
0.343       f_wlewka powierzchnia wsadu,  m^2       
0.55       h_piec   wysokosc pieca, m      
0.45       b_piec   szerokosc pieca, m       
1.34       w_piec   dlugosc pieca,  m    
0.8      coeff    emisyjnosc sciany pieca od stronie zewnetrznej                                                                         -------------
0.6      coemssp  emisyjnosc sciany pieca od stronie wewnetrznej                                                                         -------------
39       NX1      liczba elementow w kierunku X1, grubosc    |         
1        NX2      liczba elementow w kierunku X2, szerokosc  |           
1        NX3      liczba elementow w kierunku X3, dlugosc    |
10        DNP2     calkowita liczba punktow interpolacji warunkow brzegowych                    
C-----------------------------------------------------------------------
C        X1 - wspolrzedne wezlow                      mm   -------------
C Wezel No:  1,     2,    3,   ....,     NX1+1             -------------
C---------   --   --   --  --  --   --   --  --   ----------------------
0.    .1     0.4   1.      3.    7.   15.   20.   30.    40.    50. 
60.   70.    80.  90.    100.  110.  120.  130.  140.   150.   160. 
170.  180.  190.  200.   210.  215.  218.  219.  219.6  219.9  220.
230.  236.  
238.  239.  240.  241.   242.
C-----------------------------------------------------------------------
C        X2 - wspolrzedne wezlow                      mm   -------------
C Wezel No:  1,     2,    3,   ....,     NX2+1             -------------
C---------   --   --   --  --  --   --   --  --   ----------------------
  0.    1000.
C-----------------------------------------------------------------------
C        X3 - wspolrzedne wezlow                      mm   -------------
C Wezel No:  1,     2,    3,   ....,     NX3+1             -------------
C---------   --   --   --  --  --   --   --  --   ----------------------
  0.    1000.
C-----------------------------------------------------------------------      
3      nt_pom   liczba punktow sledzenia temperatury                    
C-----------------------------------------------------------------------
C        wspolrzedne X1, X2, X3 punktow sledzenia temperatury; w kolejnych wierszach
C Punkt No:  1,     2,    3,   ...., do    nt_pom               -------------
C---------   --   --   --  --  --   --   --  --   ----------------------  
  0.    130.  242.
  500.   500.  500. 
  500.   500.  500.      
C-----------------------------------------------------------------------
C        wspolrzedne punktow interp. temp. otoczenia    min. -----------
C Punkt No:  1,     2,    3,   ....,    DNP2               -------------
C---------   --   --   --  --  --   --   --  --   ----------------------
   0.  3.0   17.8  30.3   57.8  120.2  198.5   250.0  287.6  333.0  414.0
C-----------------------------------------------------------------------
C Mnożnik wspolczynnik wymiany ciepla na zewnetrznej scianie pieca (nad plyta)                        --
C przedzial No: 1,  2,    3,   ....,    DNP2                ------------
C-----------------------------------------------------------------------
 1. 1.  1.   1. 1.  1.  1. 1.  1.  1. 1.  1.
C-----------------------------------------------------------------------
C Mnoznik wspolczynnika wymiany ciepla i predkosc spalin (m/s) od strony pieca (dol plyty)                       --
C przedzial No: 1,  2,    3,   ....,    DNP2               ------------
C-----------------------------------------------------------------------
  1.    1.     1.   1.    1.     1.    1.    1.     1.    1.    1.     1.
  0.    0.     0.   0.    0.     0.    0.    0.     0.    0.    0.     0. 
C-----------------------------------------------------------------------
C Temperatura otoczenia (nad plyta) i temperatura spalin (pod plyta)   C deg.  
C Punkt No:  1,     2,    3,   ....,    DNP2
C---------   --   --   --  --  --   --   --  --   ---------------------
  16.   17.   17.5   18. 18.3  18.8  19.7     20.  20.5   23.     23.  
  20.   200.   400. 500. 600.  800.  1000.  1102.  1120.  1120.  1120.
C-----------------------------------------------------------------------
C Przewodnosc cieplna warstwy elementow          W/m/K       -----------
C Element nr:  1,     2,    3,   ....,    NX1                -----------
C---------   --   --   --  --  --   --   --  --   ----------------------
 28.    28.    28.    28.    28.    28.    28.    28.    28.    28.   28. 
 28.    28.    28.    28.    28.    28.    28.    28.    28.    28.   28.
 28.    28.    28.    28.    28.    28.    28.    28.    28.    28.   28.
 28.    28.
 20.    20.    20.    20.    20.  
C-----------------------------------------------------------------------
C Gestosc materialu warstwy elementow            kg/m^3    -------------
C Element nr:  1,     2,    3,   ....,    NX1               ------------
C---------   --   --   --  --  --   --   --  --   ----------------------
 130.   130.   130.   130.   130.  130.  130.  130.  130.  130.  130. 
 130.   130.   130.   130.   130.  130.  130.  130.  130.  130.  130. 
 130.   130.   130.   130.   130.  130.  130.  130.  130.  130.  130.
 130.   130.
 130.   130.   130.   130.   130.  
C-----------------------------------------------------------------------
C Cieplo wlasciwe materialu warstwy elementow J/K*kg                  --
C Element nr: 1,     2,    3,   ....,    Nx1;               ------------
C---------   --   --   --  --  --   --   --  --   ----------------------
1040.  1040.  1040.  1040.  1040.  1040.  1040.  1040.  1040.  1040.  1040. 
1040.  1040.  1040.  1040.  1040.  1040.  1040.  1040.  1040.  1040.  1040. 
1040.  1040.  1040.  1040.  1040.  1040.  1040.  1040.  1040.  1040.  1040. 
1040.  1040.
1040.  1040.  1040.  1040.  1040.  
C-END OF FILE-----------------------------------------------------------
 
