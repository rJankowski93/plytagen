# PŁYTA-KPPC

Generator pliku wsadowego i wizualizacji dla programu Płyta 3D. Projekt na zaliczenie przedmiotu Komputerowe Projektowanie Procesów Cieplnych.

## Contributors

* (sz)ympeg
* rafi

## technologia

* HTML + CSS + nw.js

## TODO

1. uzupełnienie generowania pliku (generate_file.js) - brakuje tam tych wartości ostatnich (lambdy, gęstości itd)
2. ekran 6 szerokość

dalej:
- uzupełnić opisy
- jak to z tymi kropkami
- jak to z tymi minusami (warunek na prędkość >100 itp)
- wstępne testy i bugfixy na bieżąco

jeszcze dalej:
- pakowanie do .exe
- testy, bugfixy 

